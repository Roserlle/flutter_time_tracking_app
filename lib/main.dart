import 'package:flutter/material.dart';

import '/screens/login_screen.dart';
import '/screens/meinekonto_screen.dart';
import '/screens/visitenkarte_screen.dart';
import '/screens/zeiterfassung_screen.dart';
import 'screens/arbeitszeit.dart';

void main() {
    runApp(MyApp());
}

class MyApp extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
        return MaterialApp(
            debugShowCheckedModeBanner: false,
            title: 'Time Tracking App',
            routes: {
                '/' : (context) => LoginScreen(),
                '/meine-konto' : (context) => MeineKontoScreen(),
                '/visitenkarte' : (context) => VisitenKarteScreen(),
                '/zeiterfassung' : (context) => ZeiterfassungScreen(),
                '/time-record' : (context) => AddTimeRecordScreen()
            },
        );
    }
}