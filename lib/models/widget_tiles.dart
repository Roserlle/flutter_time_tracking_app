class WidgetTiles {
    const WidgetTiles({required this.image, required this.text});

    final String image;
    final String text;
}