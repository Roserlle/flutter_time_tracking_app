import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';


AppBar appBar = AppBar(
    backgroundColor: Colors.white,
    iconTheme: IconThemeData(color: Colors.black),
    toolbarHeight: 100,
);

ButtonStyle btnDefaultTheme = ElevatedButton.styleFrom(
    primary: Colors.white,
    shadowColor: Colors.transparent,
);

TextStyle accountHeadings = TextStyle(
    fontSize: 22,
    height: 2,
    fontFamily: 'Allerta',
    fontWeight: FontWeight.w400,
    color: Colors.black
);

TextStyle accountHeadings2 = TextStyle(
    fontFamily: 'Allerta',
    fontWeight: FontWeight.w400,
    fontSize: 16,
    height: 2
);

TextStyle kontaktTextStyle = TextStyle(
    fontFamily: 'Allerta',
    fontWeight: FontWeight.w400, 
    fontSize: 12
);

TextStyle adresseTextStyle = TextStyle(
    fontFamily: 'Mulish',
    fontWeight: FontWeight.w400, 
    fontSize: 12,
    color: HexColor('##191D26'),
);