import 'package:flutter/material.dart';

import '../utils/themes.dart';

class AnderoDetailsCard extends StatelessWidget {

    Widget imgGreg = Container(
        child: Image.asset('assets/ava.png', width: 64, height: 64),
    );

    Widget name = Text(
        'Andero Musterman',
        style: accountHeadings2,
    );

    Widget email = Text(
        'andero.mustermann@f-bootcamp.com',
        style: TextStyle(
            fontFamily: 'Mullish',
            fontSize: 12,
            decoration: TextDecoration.underline
        ),
    );

    Widget monteur = Padding(
        padding: EdgeInsets.only(top: 10),
        child: Text(
            'Abteilungsleiter ',
            style: TextStyle(
                fontFamily: 'Mullish',
                fontSize: 12,
            ),
        )
    );

    @override
    Widget build(BuildContext context) {
        return Row(
            children: [
                imgGreg,
                Container(
                    padding: EdgeInsets.only(left: 20),
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                            name,
                            email,
                            monteur
                        ],
                    ),
                )
            ],
        );
    }
}