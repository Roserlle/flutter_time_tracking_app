import 'package:flutter/material.dart';
import 'package:time_tracking_app/utils/themes.dart';

class FlamingoDetails extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
        return Row(
            children: [
                Image.asset('assets/flamingo.png', width: 64, height: 64),
                Container(
                    padding: EdgeInsets.only(left: 20),
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                            Text(
                                'Ingo Flamingo',
                                style: accountHeadings2,
                            ),
                            Text(
                                'ingo.flamingo@f-bootcamp.com',
                                style: TextStyle(
                                    fontFamily: 'Mullish',
                                    fontSize: 12,
                                ),
                            ),
                            Padding(
                                padding: const EdgeInsets.only(top: 5),
                                child: Container(
                                    padding: EdgeInsets.all(5),
                                    decoration: BoxDecoration(
                                        color: Colors.black,
                                        border: Border(
                                            left: BorderSide(
                                                color: Colors.purple,
                                                width: 4
                                            )
                                        )
                                    ),
                                    child: Text(
                                        '0160-123456789',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 12,
                                            fontFamily: 'Allerta'
                                        ),
                                    ),
                                ),
                            )
                        ],
                    ),
                )
            ],
        );
    }
}