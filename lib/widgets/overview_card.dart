import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

import '../utils/themes.dart';

class UbersichtCard extends StatelessWidget {
    const UbersichtCard({ Key? key }) : super(key: key);

    @override
    Widget build(BuildContext context) {
        return Card(
            child: Container(
                width: double.infinity,
                padding: const EdgeInsets.fromLTRB(30, 0, 30, 30),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                        Text('Ubersicht 2021', style: accountHeadings,),
                        Padding(
                            padding: const EdgeInsets.fromLTRB(0, 15, 0, 10),
                            child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [Text('Jaresurlaub'), Text('25')],
                            ),
                        ),
                        Padding(
                            padding: const EdgeInsets.only(bottom: 10),
                            child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [Text('Resturlaub EPOS'), Text('10')],
                            ),
                        ),
                        Padding(
                            padding: const EdgeInsets.only(bottom: 10),
                            child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [Text('Beantragt'), Text('08')],
                            ),
                        ),
                        Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [Text('Ubertrag Vorjahr'), Text('01')],
                        ),
                        Text(
                            '(gultig bis 31.03.2021)',
                            style: TextStyle(
                                fontFamily: 'Mullish',
                                fontSize: 12,
                                color: HexColor('#191D26')
                            ),
                        ),
                        SizedBox(
                            width: 200,
                            child: Container(
                                margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
                                padding: EdgeInsets.only(left: 15),
                                decoration: BoxDecoration(color: Colors.black),
                                child: Center(
                                    child: Row(
                                        children: [
                                            TextButton(
                                                onPressed: (){}, 
                                                child: Text(
                                                    'Urlaub beantragen',
                                                    style: TextStyle(
                                                        color: Colors.white,
                                                        fontWeight: FontWeight.w500,
                                                    ),
                                                    
                                                )
                                            ),
                                            Icon(
                                                Icons.add,
                                                color: Colors.white,
                                            )
                                        ],
                                    ),
                                ),
                            ),
                        )
                    ],
                ),
            ),
        );
    }
}