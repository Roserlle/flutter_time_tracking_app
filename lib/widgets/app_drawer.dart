import 'package:flutter/material.dart';

import '../models/widget_tiles.dart';
import '../screens/meinekonto_screen.dart';
import '../screens/visitenkarte_screen.dart';
import '../screens/zeiterfassung_screen.dart';
import '../utils/themes.dart';
import 'drawer_head.dart';

class AppDrawer extends StatefulWidget {
    @override
    _AppDrawerState createState() => _AppDrawerState();
}

class _AppDrawerState extends State<AppDrawer> {
    int _selectedIndex = 2;

    void _onSelected(int index) {
        setState(() {
            _selectedIndex = index;
        });
    }

    final List<WidgetTiles> widgetTilesList = [
        WidgetTiles(image: 'assets/user.png', text: 'Mein Konto'),
        WidgetTiles(image: 'assets/visitenkarte.png', text: 'VisitenKarte'),
        WidgetTiles(image: 'assets/zeiterfassung.png', text: 'Zeiterfassung'),
        WidgetTiles(image: 'assets/einzatse.png', text: 'Meine Einsatze'),
    ];

    @override
    Widget build(BuildContext context) {

        void selectedItem(BuildContext context, int ind) {
            Navigator.of(context).pop();

            switch (ind) {
            case 0:
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => MeineKontoScreen(),
                ));
                break;
            case 1:
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => VisitenKarteScreen(),
                ));
                break;
            case 2:
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => ZeiterfassungScreen(),
                ));
                break;
            case 3:
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => ZeiterfassungScreen(),
                ));
                break;    
            }
        }

        return SizedBox(
            width: double.infinity,
            child: Drawer(
                child: SingleChildScrollView(
                    child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                            DrawerHeadWidget(),
                            ListView.builder(
                                shrinkWrap: true, 
                                itemCount: widgetTilesList.length,
                                itemBuilder: (listViewContext, index){
                                    return Container(
                                        margin: EdgeInsets.fromLTRB(20, 0, 0, 0),
                                        child: Align(
                                            alignment: Alignment.centerLeft,
                                            child: ElevatedButton(
                                                style: btnDefaultTheme,
                                                onPressed: (){
                                                    _onSelected(index);
                                                    setState(() { 
                                                        selectedItem(context, index);
                                                    });
                                                }, 
                                                child: Column(
                                                    children: [
                                                        Padding(
                                                            padding: const EdgeInsets.all(8.0),
                                                            child: Image.asset(
                                                                widgetTilesList[index].image, 
                                                                width: 60, 
                                                                height: 60,
                                                                color: _selectedIndex == index
                                                                ? Colors.black
                                                                : Color(0xFF0E3311).withOpacity(0.5),
                                                            ),
                                                        ),
                                                        Text(
                                                            widgetTilesList[index].text, 
                                                            style: TextStyle(
                                                                color: _selectedIndex == index
                                                                ? Colors.black
                                                                : Color(0xFF0E3311).withOpacity(0.5),
                                                            )
                                                        )
                                                    ],
                                                )
                                            ),
                                        ),
                                    );
                                },
                            )
                        ],
                    ),
                ),
            ),
        );
    }
}