import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:time_tracking_app/widgets/flamingo_details.dart';

import '../utils/themes.dart';
import '../widgets/flamingo_details.dart';
import '../widgets/greg_details_card.dart';
import '../widgets/overview_card.dart';


class MeineKontoCard extends StatelessWidget {

    Widget imgCalendar = Container(
        child: Image.asset('assets/calendar_icon.PNG', width: 50, height: 55),
    );

    Widget actualBudgetCard = SizedBox(
        height: 95,
        child: Container(
            decoration: BoxDecoration(
                color: HexColor('#E0E0E0')
            ),
            padding: EdgeInsets.fromLTRB(30, 0, 30, 0),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                    Text(
                        'Aktuelles Budget',
                        style: TextStyle(
                            fontFamily: 'Mulish',
                            fontWeight: FontWeight.w600,
                            fontSize: 14
                        ),
                    ),
                    Stack(
                        children: [
                            Container(
                                child: Image.asset('assets/icon_bg.png'),
                                
                            ),
                            Positioned(
                                left: 16,
                                top: 10,
                                child: Text('7', style: TextStyle(
                                    fontFamily: 'Allerta',
                                    fontSize: 16,
                                    color: Colors.white
                                ))
                            )
                        ],
                    )
                ],
            ),
        ),
    );


    @override
    Widget build(BuildContext context) {
        return Padding(
            padding: const EdgeInsets.fromLTRB(5, 8, 5, 0),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                    Card(
                        child: Container(
                            padding: const EdgeInsets.fromLTRB(30, 0, 30, 30),
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                    Container(
                                        padding: EdgeInsets.only(bottom: 10),
                                        child: Text('Mein Konto', style: accountHeadings),
                                    ),
                                    GregDetailsCard(),
                                    Container(
                                        padding: EdgeInsets.fromLTRB(0, 20, 0, 10),
                                        child: Text('Ansprechpartner', style: accountHeadings),
                                    ),
                                    FlamingoDetails(),
                                    Container(
                                        padding: EdgeInsets.fromLTRB(0, 20, 0, 10),
                                        child: Text('Wochenbericht', style: accountHeadings),
                                    ),
                                    Row(
                                        children: [
                                            imgCalendar,
                                            Column(
                                                children: [
                                                    Container(
                                                        padding: EdgeInsets.fromLTRB(35, 0, 0, 15),
                                                        child: Text('12.03 - 13.03.2021', style: accountHeadings2),
                                                    ),
                                                ],
                                            ),
                                        ],
                                    ),
                                    Container(
                                        margin: EdgeInsets.only(left: 80),
                                        decoration: BoxDecoration(color: Colors.black),
                                        child: Row(
                                            children: [
                                                TextButton(
                                                    onPressed: (){},
                                                    child: Text(
                                                        'Wochenbericht zuschicken ',
                                                        style: TextStyle(
                                                            color: Colors.white,
                                                            fontSize: 14
                                                        ),
                                                    ),
                                                ),
                                                Icon(
                                                    Icons.send_sharp,
                                                    color: Colors.white,
                                                    size: 10
                                                ),
                                            ],
                                        ),
                                    ),
                                    Container(
                                        padding: EdgeInsets.fromLTRB(0, 20, 0, 10),
                                        child: Text('Monatsbericht', style: accountHeadings),
                                    ),
                                    Row(
                                        children: [
                                            imgCalendar,
                                            Column(
                                                children: [
                                                    Container(
                                                        padding: EdgeInsets.fromLTRB(35, 0, 0, 15),
                                                        child: Text('April 2020 ', style: accountHeadings2),
                                                    ),
                                                ],
                                            ),
                                        ],
                                    ),
                                    Container(
                                        margin: EdgeInsets.only(left: 80),
                                        decoration: BoxDecoration(color: Colors.black),
                                        child: Center(
                                            child: Row(
                                                children: [
                                                    TextButton(
                                                        onPressed:(){},
                                                        child: Text(
                                                            '  Monatsbericht erstellen  ',
                                                            style: TextStyle(
                                                                color: Colors.white,
                                                                fontSize: 14
                                                            ),
                                                        ),
                                                    ),
                                                    Icon(
                                                        Icons.send_sharp,
                                                        color: Colors.white,
                                                        size: 10
                                                    ),
                                                ],
                                            ),
                                        ),
                                    ),
                                ],
                            ),
                        ),
                    ),
                    UbersichtCard(),
                    actualBudgetCard,
                ],
            ),
        );
    }
}
