import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:time_tracking_app/widgets/andero_details.dart';

import '/utils/themes.dart';
import '/widgets/andero_details.dart';

class VorgesetzteTab extends StatelessWidget {


    @override
    Widget build(BuildContext context) {

        Widget urlText = Center(
            child: Container(
                padding: EdgeInsets.only(bottom: 20),
                child: Text(
                    'www.flutter-bootcamp.com',
                    style: TextStyle(
                        color: HexColor('#00A4EA').withOpacity(0.64),
                        fontFamily: 'Mulish',
                        fontSize: 12
                    ),
                ),
            ),
        );

        Widget adresseDetails = Container(
            padding: EdgeInsets.fromLTRB(30, 20, 0, 0),
            child: Row(
                children: [
                    Container(
                        margin: EdgeInsets.only(left: 10),
                        child:Image.asset(
                            'assets/location.PNG',
                            width: 40,
                        )
                    ),
                    Container(
                        margin: EdgeInsets.only(left: 25),
                        padding: EdgeInsets.only(right: 5),
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children:[
                                Text(
                                    'Flutter Bootcamp',
                                    style: TextStyle(
                                        fontFamily: 'Allerta',
                                        fontWeight: FontWeight.w400, 
                                        fontSize: 16
                                    ),
                                ),
                                Text('6783 Ayala Avenue', style: adresseTextStyle),
                                Text('1200 Metro Manila', style: adresseTextStyle) 
                            ]
                        ),
                    )
                ],
            ),
        );

        Widget kontaktDetails = Container(
            padding: EdgeInsets.fromLTRB(30, 20, 0, 0),
            margin: EdgeInsets.only(bottom: 30),
            child: Row(
                children: [
                    Container(
                        margin: EdgeInsets.only(left: 10),
                        child:Image.asset('assets/contact.PNG', width: 50)
                    ),
                    Container(
                        margin: EdgeInsets.only(left: 25),
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children:[
                                Text('T: +49 1234 56 789 01', style: kontaktTextStyle),
                                Text('F: +49 1234 56 789 01-2', style: kontaktTextStyle),
                                Text('M: +49 1234 56', style: kontaktTextStyle),
                                Text('E:  andreo.mustermann@f-bo...',style: kontaktTextStyle),
                            ]
                        ),
                    ),
                ],                                                     
            ),
        );


        return SingleChildScrollView(
            child: Container(
                padding: EdgeInsets.all(10),
                child: Card(
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                            Container(
                                padding: EdgeInsets.fromLTRB(30, 15, 0, 0),
                                child: Text('Visitenkarte', style: accountHeadings),
                            ),
                            Padding(
                                padding: EdgeInsets.fromLTRB(30, 10, 0, 10),
                                child: AnderoDetailsCard(),
                            ),
                            Container(
                                padding: EdgeInsets.fromLTRB(30, 0, 30, 0),
                                child: Image.asset('assets/vector.png', width: double.infinity,),
                            ),
                            Container(
                                padding: EdgeInsets.only(left: 30),
                                child: Text('Adresse', style: accountHeadings),
                            ),
                            adresseDetails,
                            Container(
                                padding: EdgeInsets.fromLTRB(30, 15, 0, 0),
                                child: Text('Kontakt', style: accountHeadings),
                            ),
                            kontaktDetails,
                            urlText
                        ],
                    ),
                ),
            ),
        );
    }
}