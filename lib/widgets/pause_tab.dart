import 'package:flutter/material.dart';

import '/utils/themes.dart';
import '/widgets/kategorie_dropdown.dart';
import '/widgets/listwheel_data.dart';
import '/widgets/textfield_widget.dart';

class PauseTabScreen extends StatefulWidget {
    @override
    _PauseTabScreenState createState() => _PauseTabScreenState();
}

class _PauseTabScreenState extends State<PauseTabScreen> {
    String? _value;

    @override
    Widget build(BuildContext context) {

        Widget pauseContainer = Container(
            margin: EdgeInsets.only(bottom: 20),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                    Container(
                        padding: EdgeInsets.fromLTRB(30, 15, 0, 0),
                        child: Text('Pause', style: accountHeadings),
                    ),
                    Container(
                        padding: EdgeInsets.fromLTRB(30, 10, 30, 0),
                        width: double.infinity,
                        child: ListWheelScrollViewWidget(),
                    ),
                ],
            ),
        );

        return SingleChildScrollView(
            child: Container(
                padding: EdgeInsets.all(10),
                child: Column(
                    children: [
                        KategorieDropdown(),
                        pauseContainer,
                        TextFieldWidget()
                    ],
                ),
            ),
        );
    }
}