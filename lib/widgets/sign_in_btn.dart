import 'package:flutter/material.dart';

class SignInButton extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
        Widget microsoftLogo = Container(
            padding: EdgeInsets.only(left: 5),
            child: Image.asset('assets/microsoft_logo.PNG'),
        );

        Widget signInBtn = Container(
            padding: EdgeInsets.fromLTRB(15, 0, 60, 0),
            child: Text(
                'Sign in with Microsoft',
                style: TextStyle(
                    color: Colors.black,
                    fontFamily: 'Mulish',
                    fontSize: 14,
                    fontWeight: FontWeight.w600,
                    height: 2
                ),
            ),
        );

        Widget nextLogo = Container(
            child: Image.asset('assets/next_logo.png'),
        );
        return SizedBox(
            width: 302,
            height: 57,      
            child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                    primary: Colors.white,
                    elevation: 4
                ),
                onPressed: (){
                    Navigator.pushReplacementNamed(context, '/meine-konto');
                },
                child: Row(
                    children: [
                        microsoftLogo,
                        signInBtn,
                        nextLogo
                    ],
                ),
            ),
        );
    }
}