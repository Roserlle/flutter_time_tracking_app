import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

class TextFieldWidget extends StatelessWidget {
    @override
    Widget build(BuildContext context) {

        Widget commentField = SizedBox(
            height: 150,
            child: Container(
                margin: EdgeInsets.all(30),
                decoration: BoxDecoration(
                    color: HexColor('#F5F5F5'),
                    border: Border.all(
                        color: Colors.black,
                    )
                ),
                child: TextField(
                    maxLines: 5,
                    keyboardType: TextInputType.text,
                    style: TextStyle(),
                    decoration: InputDecoration(
                        icon: Padding(
                            padding: const EdgeInsets.only(left: 15),
                            child: Image.asset('assets/ava.png', width: 50, height: 50),
                        ),
                        border: InputBorder.none,
                        labelText: 'Bemerkung hinzufügen',
                        labelStyle: TextStyle(
                            fontFamily: 'Mulish',
                            fontSize: 16,
                            fontStyle: FontStyle.italic,
                        ),
                    ),
                ),
            ),
        );

        Widget abortBtn = Padding(
            padding: const EdgeInsets.only(right: 15),
            child: TextButton(
                onPressed: (){}, 
                child: Text('Abort', style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.w500
                ))
            ),
        );

        Widget savedBtn = SizedBox(
            width: 110,
            child: Container(
                margin: EdgeInsets.fromLTRB(0, 10, 10, 10),
                decoration: BoxDecoration(color: Colors.black),
                child: Center(
                    child: Row(
                        children: [
                            TextButton(
                                onPressed: (){}, 
                                child: Text(
                                    'Speichern',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.w500,
                                    ),
                                    
                                )
                            ),
                            Icon(Icons.send, color: Colors.white, size: 12)
                        ],
                    ),
                ),
            ),
        );

        return Container(
            child: Column(
                children: [
                    commentField,
                    Container(
                        margin: EdgeInsets.only(bottom: 50),
                        padding: EdgeInsets.only(right: 20),
                        child: Column(
                            children: [
                                Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                        abortBtn,
                                        savedBtn,
                                    ],
                                )
                            ],
                        ),
                    )
                ],
            ),
        );
    }
}