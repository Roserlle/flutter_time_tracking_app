import 'package:flutter/material.dart';

class ListWheelScrollViewWidget extends StatelessWidget {
    List<String> hours = [
        '12:00 Uhr', '12:15 Uhr', '12:30 Uhr', '12:45 Uhr', '1:00 Uhr', '1:15 Uhr', '1:30 Uhr', '1:45 Uhr', 
        '2:00 Uhr', '2:15 Uhr', '2:30 Uhr', '2:45 Uhr', '3:00 Uhr', '3:15 Uhr', '3:30 Uhr', '3:45 Uhr', 
        '4:00 Uhr', '4:15 Uhr', '4:30 Uhr', '4:45 Uhr', '5:00 Uhr', '5:15 Uhr', '5:30 Uhr', '5:45 Uhr', 
        '6:00 Uhr', '6:15 Uhr', '6:30 Uhr', '6:45 Uhr', '7:00 Uhr', '7:15 Uhr', '7:30 Uhr', '7:45 Uhr', 
        '8:00 Uhr', '8:15 Uhr', '8:30 Uhr', '8:45 Uhr', '9:00 Uhr', '9:15 Uhr', '9:30 Uhr', '9:45 Uhr', 
        '10:00 Uhr', '10:15 Uhr', '10:30 Uhr', '10:45 Uhr', '11:00 Uhr', '11:15 Uhr', '11:30 Uhr', '11:45 Uhr' 
    ];

    List<Widget> generateHourList() {
        List<Widget> time = [];
        hours.map((hour) {
            time.add(
                ListTile(
                    title: Text(hour, style: TextStyle(color: Colors.black)),
                )
            );
        });
        return time;
    }


    @override
    Widget build(BuildContext context) {
        return Container(
            width: double.infinity,
            child:  Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                    Container(
                        width: 120,
                        height: 120,
                        child: ListWheelScrollView(
                            itemExtent: 75,
                            perspective: 0.0000000001,
                            children: generateHourList(),
                        ),
                    ),
                    Container(
                        width: 120,
                        height: 120,
                        child: ListWheelScrollView(
                            itemExtent: 75,
                            perspective: 0.0000000001,
                            children: generateHourList(),
                        ),
                    )
                ],
            ),
        );
    }
}