import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

import '/utils/themes.dart';

class KategorieDropdown extends StatefulWidget {
    @override
    _KategorieDropdownState createState() => _KategorieDropdownState();
}

class _KategorieDropdownState extends State<KategorieDropdown> {
    String? _value;

    @override
    Widget build(BuildContext context) {
        return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
                Container(
                    padding: EdgeInsets.fromLTRB(30, 15, 0, 0),
                    child: Text('Kategorie', style: accountHeadings),
                ),
                Container(
                    padding: EdgeInsets.fromLTRB(30, 10, 30, 0),
                    width: double.infinity,
                    child: DropdownButton<String>(
                        isExpanded: true,
                        icon: Image.asset('assets/dropdown.PNG', width: 20, height: 20,),
                        items: [
                            DropdownMenuItem<String>(
                                value: 'one',
                                child: Text(
                                    'Baustellenvorbereitung',
                                    style: TextStyle(
                                        fontFamily: 'Mullish',
                                        fontWeight: FontWeight.w600,
                                        fontSize: 14,
                                    )
                                ),
                            ), 
                        ],
                        onChanged: (String? value) {
                            setState(() {
                                _value = value;
                            });
                        },
                        hint: Text(
                            'Wahlen Sie bitte Kategorie aus',
                            style: TextStyle(
                                fontFamily: 'Mullish',
                                fontWeight: FontWeight.w600,
                                fontSize: 14,
                                color: HexColor('#B9B9B9')
                            ),
                        ),
                        value: _value,
                    ),
                ),
            ],
        );
    }
}