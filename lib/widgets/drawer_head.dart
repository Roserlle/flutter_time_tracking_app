import 'package:flutter/material.dart';

import '../utils/themes.dart';

class DrawerHeadWidget extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
        return SizedBox(
            height: 150,
            child: DrawerHeader(
                padding: EdgeInsets.fromLTRB(15, 0, 35, 0),
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                        ElevatedButton(
                            onPressed: (){
                                Navigator.pop(context);
                            },
                            style: btnDefaultTheme, 
                            child: Image.asset(
                                'assets/close_icon.PNG',
                                width: 25,
                                height: 40,
                            ),
                        ),
                        Container(
                            child: Image.asset(
                                'assets/vector_51.png',
                                width: 30,
                                height: 60,
                            ),
                        )
                    ],
                )
            ),
        );
    }
}