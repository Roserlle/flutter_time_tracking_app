import 'package:flutter/material.dart';

import '/widgets/sign_in_btn.dart';

class LoginScreen extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
        Widget imgLogo = Container(
            margin: EdgeInsets.only(top:100),
            child: Image.asset(
                'assets/vector_51.png',
                width: 45,
                height: 75,
            ),
        );

        Widget lblAppTitle = Container(
            margin: EdgeInsets.fromLTRB(0, 50, 0, 40),
            width: 363,
            height: 51,
            child: Text(
                'Flutter FieldPass',
                style: TextStyle(
                    fontSize: 14,
                    fontFamily: 'Roboto',
                    height: 2
                ),
                textAlign: TextAlign.center,
            ),
        );

        Widget bottomText = Container(
            margin: EdgeInsets.only(top: 150),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                    Text(
                        'Impressum', 
                        style: TextStyle(
                            fontFamily: 'Mullish',
                            fontWeight: FontWeight.w600,
                            fontSize: 14
                        )
                    ),
                    Text(
                        'Datenschutz',
                        style: TextStyle(
                            fontFamily: 'Mullish',
                            fontWeight: FontWeight.w600,
                            fontSize: 14
                        )
                    )
                ],
            ),
        );

        return Scaffold(
            body: SingleChildScrollView(
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                        imgLogo,
                        lblAppTitle,
                        SignInButton(),
                        bottomText
                    ],
                ),
            ),
        );
    }
}