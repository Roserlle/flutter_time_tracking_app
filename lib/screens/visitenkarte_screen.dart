import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

import '/utils/themes.dart';
import '/widgets/app_drawer.dart';
import '/widgets/vorgesetzte_tab.dart';


class VisitenKarteScreen extends StatelessWidget {
    final _ktabs = <Tab>[
        Tab(text: 'Meine Visitenkarte'),
        Tab(text: 'Vorgesetzte'),
    ];

    @override
    Widget build(BuildContext context) {

        Widget urlText = Center(
            child: Container(
                padding: EdgeInsets.only(bottom: 20),
                child: Text(
                    'www.flutter-bootcamp.com',
                    style: TextStyle(
                        color: HexColor('#00A4EA').withOpacity(0.64),
                        fontFamily: 'Mulish',
                        fontSize: 12
                    ),
                ),
            ),
        );

        Widget adresseDetails = Container(
            padding: EdgeInsets.fromLTRB(30, 20, 0, 0),
            child: Row(
                children: [
                    Container(
                        margin: EdgeInsets.only(left: 10),
                        child:Image.asset(
                            'assets/location.PNG',
                            width: 40,
                        )
                    ),
                    Container(
                        margin: EdgeInsets.only(left: 25),
                        padding: EdgeInsets.only(right: 5),
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children:[
                                Text(
                                    'Flutter Bootcamp',
                                    style: TextStyle(
                                        fontFamily: 'Allerta',
                                        fontWeight: FontWeight.w400, 
                                        fontSize: 16
                                    ),
                                ),
                                Text('6783 Ayala Avenue', style: adresseTextStyle),
                                Text('1200 Metro Manila', style: adresseTextStyle) 
                            ]
                        ),
                    )
                ],
            ),
        );

        Widget kontaktDetails = Container(
            padding: EdgeInsets.fromLTRB(30, 20, 0, 0),
            margin: EdgeInsets.only(bottom: 30),
            child: Row(
                children: [
                    Container(
                        margin: EdgeInsets.only(left: 10),
                        child:Image.asset('assets/contact.PNG', width: 50)
                    ),
                    Container(
                        margin: EdgeInsets.only(left: 25),
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children:[
                                Text('T: +49 1234 56 789 01', style: kontaktTextStyle),
                                Text('F: +49 1234 56 789 01-2', style: kontaktTextStyle),
                                Text('M: +49 1234 56', style: kontaktTextStyle),
                                Text('E: greg.neu@f-bootcamp.com',style: kontaktTextStyle),
                            ]
                        ),
                    ),
                ],                                                     
            ),
        );

        return DefaultTabController(
            length: _ktabs.length,
            child: Scaffold(
                appBar: AppBar(
                    backgroundColor: Colors.white,
                    iconTheme: IconThemeData(color: Colors.black),
                    toolbarHeight: 150,
                    bottom: TabBar(
                        tabs: _ktabs,
                        labelColor: Colors.black,
                        labelStyle: TextStyle(
                            fontFamily: 'Allerta',
                            fontSize: 15,
                        )
                    ),
                ),
                drawer: AppDrawer(),
                body: Container(
                    color: Colors.grey[350],
                    child: TabBarView(
                        children: [
                            SingleChildScrollView(
                                child: Container(
                                    padding: EdgeInsets.all(10),
                                    child: Card(
                                        child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                                Container(
                                                    padding: EdgeInsets.fromLTRB(30, 15, 0, 0),
                                                    child: Text('Visitenkarte', style: accountHeadings),
                                                ),
                                                Padding(
                                                    padding: EdgeInsets.fromLTRB(30, 10, 0, 10),
                                                    child: Row(
                                                        children: [
                                                            Container( child: Image.asset('assets/ava.png', width: 64, height: 64)),
                                                            Container(
                                                                padding: EdgeInsets.only(left: 20),
                                                                child: Column(
                                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                                    children: [
                                                                        Text('Greg Neu', style: accountHeadings2),
                                                                        Text(
                                                                            'greg.neu@f-bootcamp.com',
                                                                            style: TextStyle(
                                                                                fontFamily: 'Mullish',
                                                                                fontSize: 12,
                                                                            ),
                                                                        ),
                                                                        Padding(
                                                                            padding: EdgeInsets.only(top: 10),
                                                                            child: Text(
                                                                                'Monteur',
                                                                                style: TextStyle(
                                                                                    fontFamily: 'Mullish',
                                                                                    fontSize: 12,
                                                                                ),
                                                                            )
                                                                        )
                                                                    ],
                                                                ),
                                                            )
                                                        ],
                                                    ),
                                                ),
                                                Container(
                                                    padding: EdgeInsets.fromLTRB(30, 0, 30, 0),
                                                    child: Image.asset('assets/vector.png', width: double.infinity,),
                                                ),
                                                Container(
                                                    padding: EdgeInsets.only(left: 30),
                                                    child: Text('Adresse', style: accountHeadings),
                                                ),
                                                adresseDetails,
                                                Container(
                                                    padding: EdgeInsets.fromLTRB(30, 15, 0, 0),
                                                    child: Text('Kontakt', style: accountHeadings),
                                                ),
                                                kontaktDetails,
                                                urlText
                                            ],
                                        ),
                                    ),
                                ),
                            ),  VorgesetzteTab()   
                        ],
                    ),
                ),
            ),
        );
    }
}