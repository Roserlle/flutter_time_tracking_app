import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hexcolor/hexcolor.dart';

import '/widgets/app_drawer.dart';
import 'arbeitszeit.dart';

class ZeiterfassungScreen extends StatelessWidget {
    @override
    Widget build(BuildContext context) {

        Widget calendarIconBtn = Padding(
            padding: EdgeInsets.only(right: 15),
            child: SizedBox(
                width: 50,
                height: 50,
                child: ElevatedButton(
                    onPressed: (){},
                    style: ElevatedButton.styleFrom(
                        primary: Colors.white,
                        shape: CircleBorder(
                            side: BorderSide(width: 3, color: Colors.black),
                        )
                    ),
                    child: FaIcon(
                        FontAwesomeIcons.calendarAlt, 
                        size: 20, color: 
                        Colors.black
                    ),
                ),
            ),
        );

        Widget addIconBtn = Padding(
            padding: EdgeInsets.only(right: 15),
            child: SizedBox(
                width: 50,
                height: 50,
                child: ElevatedButton(
                    onPressed: (){
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => AddTimeRecordScreen(),
                        ));
                    },
                    style: ElevatedButton.styleFrom(
                        primary: HexColor('#E0E0E0'),
                        shape: CircleBorder()
                    ),
                    child: Icon(
                        Icons.add, 
                        size: 20, color: 
                        Colors.black
                    ),
                ),
            )
        );

        return Scaffold(
            appBar: AppBar(
                centerTitle: false,
                title: Container(
                    child: Column(
                        children: [
                            Container(
                                margin: EdgeInsets.only(right: 10),
                                child: Text('Donnerstag', style: TextStyle(
                                    color: Colors.black,
                                    fontFamily: 'Allerta',
                                    fontWeight: FontWeight.w400,
                                    fontSize: 22
                                )),
                            ),
                            Row(
                                children: [
                                    Container(
                                        margin: EdgeInsets.only(top: 5),
                                        padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                                        decoration: BoxDecoration(
                                            color: Colors.black,
                                            border: Border(
                                            left: BorderSide(
                                                color: Colors.purple,
                                                width: 4
                                            )
                                        )
                                        ),
                                        child: Text(
                                            'Offen',
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 12,
                                                fontFamily: 'Allerta'
                                            ),
                                        ),
                                    ),
                                    Container(
                                        padding: EdgeInsets.fromLTRB(20, 5, 0, 0),
                                        child: Text(
                                            '12.01.2021',
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 12,
                                                fontFamily: 'Mullish',
                                                fontWeight: FontWeight.w600
                                            ),
                                        ),
                                    )
                                ],
                            ),
                        ],
                    ),
                ),
                backgroundColor: Colors.white,
                iconTheme: IconThemeData(color: Colors.black),
                toolbarHeight: 120,
                actions: [
                    calendarIconBtn,
                    addIconBtn
                ],
            ),
            drawer: AppDrawer(),
            body: Center(
                child: Text('In Progress'),
            )
        );
    }
}