import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

import '/utils/themes.dart';
import '/widgets/kategorie_dropdown.dart';
import '/widgets/listwheel_data.dart';
import '/widgets/pause_tab.dart';
import '/widgets/textfield_widget.dart';

class AddTimeRecordScreen extends StatefulWidget {
    @override
    _AddTimeRecordScreenState createState() => _AddTimeRecordScreenState();
}

class _AddTimeRecordScreenState extends State<AddTimeRecordScreen> {
    String? _value;

    final _ktabs = <Tab>[
        Tab(text: 'Arbeitszeit'),
        Tab(text: 'Pause'),
    ];

    @override
    Widget build(BuildContext context) {

        Widget projektnummerContainer = Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
                Container(
                    padding: EdgeInsets.fromLTRB(30, 15, 0, 0),
                    child: Text('Projektnummer', style: accountHeadings),
                ),
                Container(
                    padding: EdgeInsets.fromLTRB(30, 10, 30, 0),
                    width: double.infinity,
                    child: DropdownButton<String>(
                        isExpanded: true,
                        icon: Image.asset('assets/dropdown.PNG', width: 20, height: 20,),
                        items: [
                            DropdownMenuItem<String>(
                                value: 'one',
                                child: Text(
                                    '1298721398',
                                    style: TextStyle(
                                        fontFamily: 'Mullish',
                                        fontWeight: FontWeight.w600,
                                        fontSize: 14,
                                    )
                                ),
                            ), 
                        ],
                        onChanged: (String? value) {
                            setState(() {
                                _value = value;
                            });
                        },
                        hint: Text(
                            'Wahlen Sie bitte Projektnummer aus',
                            style: TextStyle(
                                fontFamily: 'Mullish',
                                fontWeight: FontWeight.w600,
                                fontSize: 14,
                                color: HexColor('#B9B9B9')
                            ),
                        ),
                        value: _value,
                    ),
                ),
            ],
        );

        Widget editBtn = Padding(
            padding: EdgeInsets.only(top: 5),
            child: SizedBox(
                width: 50,
                height: 50,
                child: ElevatedButton(
                    onPressed: (){},
                    style: btnDefaultTheme,
                    child: Image.asset('assets/edit.png', color: HexColor('#B9B9B9'),),
                ),
            )
        );

        Widget mitarbeiterContainer = Container(
            child: Column(
                children: [
                    Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                            Container(
                                padding: EdgeInsets.fromLTRB(30, 15, 0, 0),
                                child: Text('Mitarbeiter', style: accountHeadings),
                            ),
                            Container(
                                padding: EdgeInsets.fromLTRB(30, 15, 20, 0),
                                margin: EdgeInsets.only(top:10),
                                child: Row(
                                    children: [
                                        editBtn,
                                        Padding(
                                            padding: EdgeInsets.fromLTRB(5, 5, 5, 0),
                                            child: SizedBox(
                                                width: 50,
                                                height: 50,
                                                child: ElevatedButton(
                                                    onPressed: (){},
                                                    style: ElevatedButton.styleFrom(
                                                        primary: HexColor('#E0E0E0'),
                                                        shape: CircleBorder()
                                                    ),
                                                    child: Icon(Icons.add, size: 20, color: Colors.black),
                                                ),
                                            )
                                        ),
                                    ],
                                ),
                            )
                        ],
                    ),
                    Align(
                        alignment: Alignment.topLeft,
                        child: Container(
                            margin: EdgeInsets.only(left: 20),
                            child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Image.asset('assets/search_tag.png')
                            ),
                        )
                    )
                ],
            ),
        );

        Widget pauseContainer = Container(
            margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
            child: Column(
                children: [
                    Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                            Container(
                                padding: EdgeInsets.fromLTRB(30, 15, 0, 0),
                                child: Text('Pause', style: accountHeadings),
                            ),
                            Container(
                                padding: EdgeInsets.fromLTRB(30, 15, 20, 0),
                                margin: EdgeInsets.only(top:10),
                                child: Row(
                                    children: [
                                        editBtn,
                                        Padding(
                                            padding: EdgeInsets.fromLTRB(5, 5, 5, 0),
                                            child: SizedBox(
                                                width: 50,
                                                height: 50,
                                                child: ElevatedButton(
                                                    onPressed: (){},
                                                    style: ElevatedButton.styleFrom(
                                                        primary: HexColor('#6788FF'),
                                                        shape: CircleBorder()
                                                    ),
                                                    child: Icon(Icons.add, size: 20, color: Colors.white),
                                                ),
                                            )
                                        )
                                    ],
                                ),
                            )
                        ],
                    ),
                    Align(
                        alignment: Alignment.topLeft,
                        child: Container(
                            margin: EdgeInsets.only(left: 20),
                            child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Image.asset('assets/pause_pill.png')
                            ),
                        )
                    )
                ],
            ),
        );

        Widget bereitschaftszeitContainer = Container(
            margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
            child: Column(
                children: [
                    Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                            Container(
                                padding: EdgeInsets.fromLTRB(30, 5, 0, 0),
                                child: Text('Bereitschaftszeit', style: 
                                    TextStyle(
                                        fontSize: 18,
                                        height: 2,
                                        fontFamily: 'Allerta',
                                        fontWeight: FontWeight.w400,
                                        color: Colors.black
                                    )
                                ),
                            ),
                            Container(
                                padding: EdgeInsets.fromLTRB(10, 15, 15, 0),
                                margin: EdgeInsets.only(top:10),
                                child: Row(
                                    children: [
                                        editBtn,
                                        Padding(
                                            padding: EdgeInsets.fromLTRB(5, 5, 5, 0),
                                            child: SizedBox(
                                                width: 50,
                                                height: 50,
                                                child: ElevatedButton(
                                                    onPressed: (){},
                                                    style: ElevatedButton.styleFrom(
                                                        primary: HexColor('#8465FF'),
                                                        shape: CircleBorder()
                                                    ),
                                                    child: Icon(Icons.add, size: 20, color: Colors.white),
                                                ),
                                            )
                                        )
                                    ],
                                ),
                            )
                        ],
                    ),
                    Align(
                        alignment: Alignment.topLeft,
                        child: Container(
                            margin: EdgeInsets.only(left: 20),
                            child: Padding(
                                padding: EdgeInsets.all(8.0),
                                child: Image.asset('assets/bereitschaftszeit_pill.png')
                            ),
                        )
                    )
                ],
            ),
        );

        Widget wartezeitContainer = Container(
            margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
            child: Column(
                children: [
                    Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                            Container(
                                padding: EdgeInsets.fromLTRB(30, 5, 0, 0),
                                child: Text('Wartezeit', style: accountHeadings),
                            ),
                            Container(
                                padding: EdgeInsets.fromLTRB(10, 15, 15, 0),
                                margin: EdgeInsets.only(top:10),
                                child: Row(
                                    children: [
                                        editBtn,
                                        Padding(
                                            padding: EdgeInsets.fromLTRB(5, 5, 5, 0),
                                            child: SizedBox(
                                                width: 50,
                                                height: 50,
                                                child: ElevatedButton(
                                                    onPressed: (){},
                                                    style: ElevatedButton.styleFrom(
                                                        primary: HexColor('#FFB72B'),
                                                        shape: CircleBorder()
                                                    ),
                                                    child: Icon(Icons.add, size: 20, color: Colors.white),
                                                ),
                                            )
                                        )
                                    ],
                                ),
                            )
                        ],
                    ),
                    Align(
                        alignment: Alignment.topLeft,
                        child: Container(
                            margin: EdgeInsets.only(left: 30),
                            child: Text('hinzufügen oder bearbeiten', style: TextStyle(fontFamily: 'Roboto')),
                        )
                    )
                ],
            ),
        );

        Widget arbeitszeitContainer = Container(
            margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                    Container(
                        padding: EdgeInsets.fromLTRB(30, 15, 0, 0),
                        child: Text('Arbeitszeit', style: accountHeadings),
                    ),
                    Container(
                        padding: EdgeInsets.fromLTRB(30, 10, 30, 0),
                        width: double.infinity,
                        child: ListWheelScrollViewWidget(),
                    ),
                ],
            ),
        );

        return SizedBox(
            width: double.infinity,
            child: DefaultTabController(
                length: _ktabs.length,
                child: Scaffold(
                    appBar: AppBar(   
                        automaticallyImplyLeading: false,
                        backgroundColor: Colors.white,
                        centerTitle: false,
                        title: ElevatedButton(
                            onPressed: (){Navigator.pop(context);},
                            style: btnDefaultTheme, 
                            child: Image.asset('assets/close_icon.PNG', width: 25, height: 40),
                        ),
                        actions: [
                            Container(
                                padding: EdgeInsets.only(right: 35),
                                child: Image.asset('assets/vector_51.png', width: 30, height: 60),
                            )
                        ],
                        toolbarHeight: 150,
                        bottom: TabBar(
                            tabs: _ktabs,
                            labelColor: Colors.black,
                            labelStyle: TextStyle(fontFamily: 'Allerta', fontSize: 17)
                        )
                    ),
                    body: Container(
                        child: TabBarView(
                            children: [
                                SingleChildScrollView(
                                    child: Container(
                                        padding: EdgeInsets.all(10),
                                        child: Column(
                                            children: [
                                                KategorieDropdown(),
                                                projektnummerContainer,
                                                mitarbeiterContainer,
                                                arbeitszeitContainer,
                                                pauseContainer,
                                                wartezeitContainer,
                                                bereitschaftszeitContainer,
                                                TextFieldWidget()
                                            ],
                                        ),
                                    ),
                                ),
                                PauseTabScreen(),
                            ],
                        )
                    )
                ),
            ),
        );
    }
}