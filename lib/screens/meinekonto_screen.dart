import 'package:flutter/material.dart';
import 'package:time_tracking_app/widgets/meinekonto_card.dart';

import '/utils/themes.dart';
import '/widgets/app_drawer.dart';
import '/widgets/meinekonto_card.dart';

class MeineKontoScreen extends StatelessWidget {

    Widget krankheitstage = Padding(
        padding: const EdgeInsets.fromLTRB(5, 0, 5, 20),
        child: Card(
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                    Container(
                        width: double.infinity,
                        padding: EdgeInsets.fromLTRB(30, 20, 0, 10),
                        child: Text('Krankheitstage', style: accountHeadings),
                    ),
                    Padding(
                        padding: const EdgeInsets.fromLTRB(30, 15, 20, 10),
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [Text('Insgesamt'), Text('03')],
                        ),
                    ),
                    SizedBox(
                        width: 230,
                        child: Container(
                            margin: EdgeInsets.fromLTRB(30, 10, 0, 10),
                            padding: EdgeInsets.only(left: 15),
                            decoration: BoxDecoration(color: Colors.black),
                            child: Center(
                                child: Row(
                                    children: [
                                        TextButton(
                                            onPressed: (){}, 
                                            child: Text(
                                                'Krankheit einreichen',
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontWeight: FontWeight.w500,
                                                ),
                                                
                                            )
                                        ),
                                        Icon(
                                            Icons.add,
                                            color: Colors.white,
                                        )
                                    ],
                                ),
                            ),
                        ),
                    )
                ],
            ),
        )
    );

    Widget azkonto = Padding(
        padding: const EdgeInsets.fromLTRB(5, 0, 5, 0),
        child: Card(
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                    Container(
                        width: double.infinity,
                        padding: EdgeInsets.fromLTRB(30, 20, 0, 10),
                        child: Text('AZ Konto', style: accountHeadings),
                    ),
                    Padding(
                        padding: const EdgeInsets.fromLTRB(30, 15, 20, 40),
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [Text('Stunden'), Text('100 / 250')],
                        ),
                    ),
                ],
            ),
        ),
    );

    @override
    Widget build(BuildContext context) {
        return Scaffold(
            appBar: appBar,
            drawer: AppDrawer(),
            body: SingleChildScrollView(
                child: SizedBox(
                    width: double.infinity,
                    child: Padding(
                        padding: const EdgeInsets.only(bottom: 20),
                        child: Column(
                            children: [
                                MeineKontoCard(),
                                krankheitstage,
                                azkonto
                            ],
                        ),
                    )
                ),
            ),
        );
    }
}