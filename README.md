# Time Tracking App

# Setup Guide

## Project Cloning

### Clone a repository using the command line

You can use Git from the command line, or any client you like to clone your Git repository. These instructions show you how to clone your repository using Git from the terminal.

1. From the repository, click + in the global sidebar and select Clone this repository under Get to work.
2. Copy the clone command (either the SSH format or the HTTPS). If you are using the SSH protocol, ensure your public key is in Bitbucket and loaded on the local system to which you are cloning.
3. From a terminal window, change to the local directory where you want to clone your repository.
4. Paste the command you copied from Bitbucket, for example:
5. Press Enter to create your local clone.

### Clone over HTTPS

$ `git clone [https://username@bitbucket.org/teamsinspace/documentation-tests.git](https://username@bitbucket.org/teamsinspace/documentation-tests.git)`

### Clone over SSH

$ `git clone ssh://git@bitbucket.org:teamsinspace/documentation-tests.git`

```
$ git clone git@bitbucket.org:Roserlle/flutter_capstone.git
> Cloning into `Spoon-Knife`...
> remote: Counting objects: 10,done.
> remote: Compressing objects: 100%(8/8),done.
> remove: Total 10(delta 1), reused 10(delta 1)
> Unpacking objects: 100%(10/10),done.
```

NOTE: If the clone was successful, a new sub-directory appears on your local drive in the directory where you cloned your repository. This directory has the same name as the Bitbucket repository that you cloned. The clone contains the files and metadata that Git requires to maintain the changes you make to the source files.

### Run the Flutter App

1. Go back to your terminal, change the directory and run your API.

$ `cd ..`

$ `cd csp_api`

$ `dart run bin/main.dart`

`API server active at 127.0.0.1:4000.`

`Static server active at 127.0.0.1:4001.`

2. Go back to your terminal, change the directory, open your clone project, and run the flutter app.

$ `cd ..`

$ `cd time_tracking_app`

$ `flutter run`

## Package Installation

Flutter supports using shared packages contributed by other developers to the Flutter and Dart ecosystems. This allows quickly building an app without having to develop everything from scratch.

Packages are published to [pub.dev](https://pub.dev/).

### Adding a package dependency to an app

1. Open the pubspec.yaml file located inside the app folder, and add packages under dependencies.
2. Install it.
- From the terminal: Run `flutter pub get`.
- From Android Studio/IntelliJ: Click Packages get in the action ribbon at the top of pubspec.yaml.
- From VS Code: Click Get Packages located on right side of the action ribbon at the top of pubspec.yaml.
1. Import it. Add a corresponding import statement in the Dart code.
2. Stop and restart the app, if necessary.

---

# Version

Flutter 2.2.3

---

# Packages

[font_awesome_flutte](https://pub.dev/packages/font_awesome_flutter)r: ^9.1.0

Based on Font Awesome 5.15.3. Includes all free icons:

[hexcolor](https://pub.dev/packages/hexcolor): ^2.0.5

hex color plugin allows you to add hex color codes to your flutter projects

[lint](https://pub.dev/packages/lint): ^1.6.0

`lint` is a hand-picked, open-source, community-driven collection of lint rules for Dart and Flutter projects. The set of rules follows the [Effective Dart: Style Guide](https://dart.dev/guides/language/effective-dart/style).

This package can be used as a replacement for `[package:pedantic](https://github.com/dart-lang/pedantic)` for those who prefer stricter rules.

[timetable](https://pub.dev/packages/timetable) 0.2.9

Customizable, animated calendar widget including day & week views.

---

# Fonts

- AllertaStencil-Regular
- Mulish-VariableFont_wght
- Roboto-Medium

---

# Features

![Untitled](Time%20Tracking%20App%2032b6e1e886ed494494de85987f1a0dc5/Untitled.png)

![Untitled](Time%20Tracking%20App%2032b6e1e886ed494494de85987f1a0dc5/Untitled%201.png)

![Untitled](Time%20Tracking%20App%2032b6e1e886ed494494de85987f1a0dc5/Untitled%202.png)

![Untitled](Time%20Tracking%20App%2032b6e1e886ed494494de85987f1a0dc5/Untitled%203.png)

![Untitled](Time%20Tracking%20App%2032b6e1e886ed494494de85987f1a0dc5/Untitled%204.png)

![Untitled](Time%20Tracking%20App%2032b6e1e886ed494494de85987f1a0dc5/Untitled%205.png)

![Untitled](Time%20Tracking%20App%2032b6e1e886ed494494de85987f1a0dc5/Untitled%206.png)

![Untitled](Time%20Tracking%20App%2032b6e1e886ed494494de85987f1a0dc5/Untitled%207.png)

![Untitled](Time%20Tracking%20App%2032b6e1e886ed494494de85987f1a0dc5/Untitled%208.png)

![Untitled](Time%20Tracking%20App%2032b6e1e886ed494494de85987f1a0dc5/Untitled%209.png)

![Untitled](Time%20Tracking%20App%2032b6e1e886ed494494de85987f1a0dc5/Untitled%2010.png)

![Untitled](Time%20Tracking%20App%2032b6e1e886ed494494de85987f1a0dc5/Untitled%2011.png)

![Untitled](Time%20Tracking%20App%2032b6e1e886ed494494de85987f1a0dc5/Untitled%2012.png)

![Untitled](Time%20Tracking%20App%2032b6e1e886ed494494de85987f1a0dc5/Untitled%2013.png)

![Untitled](Time%20Tracking%20App%2032b6e1e886ed494494de85987f1a0dc5/Untitled%2014.png)

## Login Screen

- Users can log in using their Microsoft Account.

## My Account Screen

- Users can submit Weekly Reports.
- Users can create Monthly Reports.
- Users can apply for Vacation Leave.
- Users can track the recorded Sick Leave.
- Users can track the summary of his/her leave
    - annual leave
    - Remaining vacation EPOS
    - Requested
    - Carryover previous year

## Visitenkarte Screen

- The screen has 2 tabs (Meine Visitenkarte and Vorgesetzte).
- Under Meine Visitenkarte tab (it is the Overview of the user's details), it includes the following:
    - Visitenkarte - User's name, email and QR image
    - Adresse
    - Kontakt
- Under Vorgesetzte tab (it is the Overview of the details of the superior), it includes the following:
    - Vorgesetzte - Superior's name, email and QR image
    - Adresse
    - Kontakt

## Zeiterfassung Screen

- It shows the current weekday
- By clicking the Calendar Icon Button, the Calendar screen will be displayed.
- Users can add events to their timetable.
- under Arbeitszeit tab, users can select the following:
    - Category
    - Project Number
    - Employee
    - Working Time
    - Standby Time
    - Period Time
- Users can abort and pause the selected time.